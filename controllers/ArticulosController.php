<?php

namespace app\controllers;
use yii\data\Pagination;

use Yii;
use app\models\Articulos;
use app\models\Preguntas;
use app\models\PreguntasQuery;
use app\models\ArticulosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Categorias;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

// use yii\base\InvalidParamException;
// use yii\web\BadRequestHttpException;
// use yii\data\Pagination;
// use yii\data\ActiveDataProvider
/**
 * ArticulosController implements the CRUD actions for Articulos model.
 */
class ArticulosController extends Controller
{
    public function behaviors()
    {
        return [

        'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index','preguntas'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['create','index','preguntas','update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [ 'actions' => ['update'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            // $model = new Articulos;
                            $id = $_GET['id'];
                            // $id = $_GET['id'];
                            $model = $this->findModel($id);
              if (Yii::$app->user->isGuest) {
                              return false;
                            }
                            // return Yii::$app->user->identity->getId() == $model->id_usu;
                            return Yii::$app->user->identity->getId() == $model->id_usu;
                        }
                        ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Articulos models.
     * @return mixed
     */
    public function actionIndex()
    {
          $categories = Categorias::find()->select(['id', 'categoria'])->where(['padre' => 0,])->all();
        $query = Articulos::find()->where(['id_usu' => Yii::$app->user->identity->getId()]);
        if (isset($_GET['cat'])) { $query = Articulos::find()->where(['id_cat' => $_GET['cat'], 'id_usu' => Yii::$app->user->identity->getId()]); }
 
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize'=>6]);

        $models = $query->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('index', [
            'categories'=> $categories, 
                                'models'=> $models, 
                                'pages' => $pages
        ]);
    }

    /**
     * Displays a single Articulos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    { $preguntas= new Preguntas;
      $mpreguntas = Preguntas::find()->where(['id_art' => $_GET['id'],])->all(); 
        // $preguntas->save();
        
        // var_dump($_POST) ;
        if ($preguntas->load(Yii::$app->request->post()) && $preguntas->save() && $preguntas->validate()) {
          Controller::refresh();
        }
        return $this->render('view', [ 'model' => $this->findModel($id), 'preguntas' => $preguntas, 'mpreguntas' => $mpreguntas,
        ]);

    }

    public function actionCreate()
    {   
        $model = new Articulos();
        $categorias = Categorias::find()->asArray()->all();
        $categoria = ArrayHelper::map($categorias, 'id', 'categoria');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->foto1 = UploadedFile::getInstance($model,'foto1')) { if ($model->upload()) { $model->save(); }
                }
          if ($model->foto2 = UploadedFile::getInstance($model,'foto2')) { if ($model->upload2()) { $model->save(); }
                }
            return $this->redirect(['view', 'id' => $model->id]); }
         else {
            return $this->render('create', [
                'model' => $model,
                'categoria'=>$categoria,
            ]);
        }
    }
    /**
     * Creates a new Articulos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

// SELECT * FROM preguntas as P  INNER JOIN articulos as A ON P.id_art = A.id  INNER JOIN user as U ON A.id_usu = U.id WHERE P.id_art = A.id

//SELECT U.username, P.post, C.comment FROM comentarios as C  INNER JOIN post as P ON C.idpost = P.idpost  INNER JOIN usuarios as U ON P.id_usu = U.id WHERE C.idpost = P.idpost



// SELECT c.preguntas, p.articulos, r.user FROM preguntas cINNERJOIN articulos pON c.id = p.id_art INNER JOIN usuarios r ON r.Id = p.id_usu
//SELECT c.comuna, p.provincia, r.region FROM Comunas cINNER JOIN Provincias pON c.idprovincia= p.idprovincia INNER JOIN Regiones r ON r.IdRegion = p.IdRegion
      // $misarticulos = Articulos::find()->select('id')->where(['id_usu' =>Yii::$app->user->identity->getId()])->orderBy('id')->all(); 
      public function actionPreguntas()    { 
      $preguntas = new Preguntas;
// echo "<br><br><br><br><br>";
      // var_dump($_POST['Preguntas']);

      if (isset($_POST['Preguntas'])) {
$model = $this->findPreguntas($_POST['Preguntas']['id']);
            // $model = Preguntas::find($_POST['Preguntas']['id']);
            $model->respuesta = $_POST['Preguntas']['respuesta'];
            $model->save();  
      }
$my=Preguntas::findBySql('select P.id, P.pregunta, P.respuesta from preguntas as P INNER JOIN articulos as A ON P.id_art = A.id where respuesta IS NULL AND id_usu ='.Yii::$app->user->identity->getId())->all();

        return $this->render('preguntas', [
                                // 'mpreguntas'=> $mpreguntas, 
                                'preguntas' => $preguntas,
                                'my' => $my,
        ]);
    }

    /**
     * Updates an existing Articulos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $categorias = Categorias::find()->asArray()->all();
        $categoria = ArrayHelper::map($categorias, 'id', 'categoria');
        $model = $this->findModel($id);
        echo "<br><br><br><br>";

        if (!empty($_POST)) {
          $file = \yii\web\UploadedFile::getInstance($model, 'foto1');
          if ($model->save()) {
              if ($file==null) {
              $fotoa = $model->foto1;
               $model->load(Yii::$app->request->post());
              $model->foto1 =$fotoa;
               $model->save();
            }
            else {
               $model->load(Yii::$app->request->post());
                $rnd= rand(0,9999);
                // $model->foto1 = $rnd.$_POST['Articulos']['foto1'];
              $model->foto1= $rnd.$file->name;
              $model->save();
              $file->saveAs('images/articulos/'.$rnd.$file->name);
              }
            }
          return $this->redirect(['view', 'id' => $model->id]);
        }
        // its better if you relegate such a code to your model class
    // }
    return $this->render('update', ['model'=>$model, 'categoria' => $categoria]);
    }

    /**
     * Deletes an existing Articulos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Articulos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Articulos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Articulos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
      protected function findPreguntas($id)
    {
        if (($model = Preguntas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
