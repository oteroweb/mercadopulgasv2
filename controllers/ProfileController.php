<?php
namespace app\controllers;

// use dektrium\user\Finder;
// use Yii;
use app\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use dektrium\user\controllers\ProfileController as BaseProfileController;

class ProfileController extends BaseProfileController
{
        public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['index'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['show'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['trayectoria'], 'roles' => ['@']],
                ],
            ],
        ];
    }

       public function actionShow($id)
    {
        $profile = $this->finder->findProfileById($id);

        if ($profile === null) {
            throw new NotFoundHttpException('No existe Perfil de Vendedor');
            // return "No existe Usuario";
        }
        $profile2= $this->findPerfil($_GET['id']);

        return $this->render('show', [
            'profile' => $profile,
             'profile2' => $profile2,
        ]);
    }

      public function actionTrayectoria($id)
    {
        $profile = $this->finder->findProfileById($id);

        if ($profile === null) {
            throw new NotFoundHttpException('No existe Perfil de Vendedor');
            // return "No existe Usuario";
        }
        $profile2= $this->findPerfil($_GET['id']);

        return $this->render('trayectoria', [
            // 'profile' => $profile,
             // 'profile2' => $profile2,
        ]);
    }

     protected function findPerfil($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}



 
