<?php  
namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\User;

use dektrium\user\controllers\AdminController as BaseAdminController;

class UsuariosController extends BaseAdminController
{
 public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['perfil', 'signup'],
                'rules' => [
                    [
                        'actions' => ['perfil'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                //     [
                //         'actions' => ['logout'],
                //         'allow' => true,
                //         'roles' => ['@'],
                    // ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionPerfil()
    { 	
    	$user= new User;

		// $user = User::find()->where(['id' => $_GET['id']]);
		// $user = User::find();

       return $this->render('perfil', 
                                array(
                                'user'=> $user, 
                                // 'query'=> $query, 
                                // 'pages' => $pages
                                )
                            );
    }
}
 ?>