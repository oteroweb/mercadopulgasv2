<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
// use app\widgets\Alert;
use yii\bootstrap\Alert;
use yii\bootstrap\Carousel;
use yii\helpers\Url;
use  yii\helpers\BaseUrl;
AppAsset::register($this);
$this->beginBlock('search');
// ActiveForm::begin();
echo '<form class="navbar-form navbar-left forbusqueda" action=""><div class="form-group boxbusqueda"><input type="text" class="form-control busqueda" placeholder="Buscar Articulos"></div><button style="font-size: 21px; padding: 0px 5px;" class="btn btn-info btn-sm" type="submit"><i class="glyphicon glyphicon-globe"></i></button></form>';
// echo Html::submitButton('Submit', ['class'=> 'btn btn-primary']);
// ActiveForm::end();
$this->endBlock();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>


<link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
<link rel="manifest" href="images/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link type="text/css" href="css/mystyle.css" rel="stylesheet">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

  <div class=" clearfix "> </div>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([

         // <img class="hidden-xs" src="http://placehold.it/150x50&text=Logo" alt="">
        // <img class="visible-xs" src="http://placehold.it/120x40&text=Logo" alt="">    
        'brandLabel' => Html::img('@web/images/logo.svg', ['alt'=>'some', 'class'=>'img-resposive']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-MP navbar-fixed-top',
        ],
    ]);
    echo $this->blocks['search'];
    $menuItems = [
        // ['label' => 'Home', 'url' => ['/site/index']],
        // ['label' => 'Sobre Nosotros', 'url' => ['/site/about']],
        // ['label' => 'Contact', 'url' => ['/site/contact']],
        // ['label' => Yii::t('app', "Login"), 'items' => $this->blocks['search']],
        // ['label' => 'Vender', 'url' => ['/articulos/create']],
    ];
    if (Yii::$app->user->isGuest) {
       $menuItems[] = ['label' => 'Registro', 'url' => ['/user/registration/register']];
        $menuItems[] = ['label' => Yii::t('app','Inicio Sesion'), 'url' => ['/user/security/login']];
    } else {

        $menuItems[] = [
                        'label' => Yii::t('app','Mi cuenta'),
            'items' => [ 
              [
                'label' => Yii::t('app','Mi Perfil'), 
                'url' => ['/user/settings/account'],
              ],
              [
              'label' => Yii::t('app','Vender'),
              'url' => ['/articulos/create'],
              ],
              [
              'label' => Yii::t('app','Mis Articulos'),
              'url' => ['/articulos/index'],
              ],
              [
              'label' => Yii::t('app','Mis Preguntas'),
              'url' => ['/articulos/preguntas'],
              ],
                [
              'label' => Yii::t('app','Mi Trayectoria'),
              'url' => ['/user/profile/trayectoria', 'id'=> Yii::$app->user->identity->getId()],
              ],
              [
              'label' => Yii::t('app','Cerrar Sesion'),
              // ."(". Yii::$app->user->identity->username .")"
              'url' => ['/user/security/logout'],
              'linkOptions' => ['data-method' => 'post']
              ],
            ],
        ];
        if(Yii::$app->user->identity->isAdmin) {
          $menuItems[] = ['label' => 'admin', 'url' => ['/user/admin/index']];
        }


    }
    echo Nav::widget([
        'options' => ['class' => 'cabeceraboton navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
  <?php 
  if (Yii::$app->controller->id == 'site') {
      # code...
echo "<br><br><br> ".Carousel::widget([
    'items' => [ 
        Html::img('@web/images/slide/liston.jpg'),
        // Html::img('@web/images/slide/liston1.jpg'),
        Html::img('@web/images/slide/liston2.jpg'),
        Html::img('@web/images/slide/liston3.jpg'),
        Html::img('@web/images/slide/liston4.jpg'),
        // Html::img('@web/images/slide/liston5.jpg'),
          ]
   
]); 

  }

 ?>


<div class=" clearfix " style="height: 67px;"> </div>
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?php //echo Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<div class=" clearfix "> </div>
<br><br><br><br><br>
<footer class="footer row">
    <!-- <div class=""> -->
        <div class="col-md-6 pull-left "> <p class="copyright"> Copyright &copy;  2015 Mercadopulgas Venezuela  </p></div>
      <!-- <div class="col-md-2"></div> -->
        <div class="col-md-4 impact developer text-right txtblack">
         <!-- <div class="col-md-3 ima pull-right developer"> -->
        Pagina Desarrollada por &nbsp;
        </div>
        <div class="col-md-2" > 
        <?= Html::a( Html::img('@web/images/owlogo.png',['class'=>'img-responsive']), 'http://oteroweb.com.ve') 
        ?> 
        </div>
    <!-- </div> -->
</footer>
<div class="row espaciofooter text-center"> 


</div>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?49gsUhBrrem76tMAt42pfZuCJij94hOY";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
