<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Articulos */
?>
<div class="articulos-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'articulo',
            'subtitulo:ntext',
            'foto1',
            'foto2',
            'estado',
            'ventas',
            'stock',
            'precio',
            'tipopago',
            'id_usu',
            'descripcion:ntext',
            'garantia',
            'tipopublicacion',
            'fechaexp',
            'fechainc',
            'id_cat',
        ],
    ]) ?>

</div>
