<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Articulos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articulos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'articulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subtitulo')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'foto1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ventas')->textInput() ?>

    <?= $form->field($model, 'stock')->textInput() ?>

    <?= $form->field($model, 'precio')->textInput() ?>

    <?= $form->field($model, 'tipopago')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_usu')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'garantia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipopublicacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechaexp')->textInput() ?>

    <?= $form->field($model, 'fechainc')->textInput() ?>

    <?= $form->field($model, 'id_cat')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
