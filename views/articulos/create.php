<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Articulos */

$this->title = Yii::t('app', 'Create Articulos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Articulos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articulos-create">

    <h1 class="titulocrear"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categoria'=>$categoria,
    ]) ?>

</div>
