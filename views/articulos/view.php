<?php
use app\models\Preguntas;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model frontend\models\Articulos */

$this->title = $model->articulo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Articulos'), 'url' => ['index']];

$this->params['breadcrumbs'][] = 

['label' => $model->getIdCat()->one()->categoria, 'url' => ['site/index', 'cat' => $model->id_cat]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articulos-view">
    <div class="row">
        <div class="col-md-3 text-right"> 
       <?php if (!isset($model)): ?>
           <?php if (Yii::$app->user->identity->getId() == $model->id_usu ): ?>
            <?php echo Html::a('<i class="glyphicon glyphicon-chevron-left iconback"></i>', ['articulos/index']) ?> 
            <?php endif ?>
       <?php endif ?>
        </div>
        <div class="col-md-6"> 
            <?php if (!$model->foto1): ?>
                <div class="art_foto1">  
                <?= Html::img('@web/images/sinfoto.png',['width'=>'100%','height'=>'100%','title'=>$model->articulo]);?>
                </div>
            <?php else: ?>
                <div class="art_foto1"> 
                <?= Html::img('@web/images/articulos/'.$model->foto1,['width'=>'100%','height'=>'100%','title'=>$model->articulo]);?>
                </div>
            <?php endif ?>                
            <div class="row art_usuario impact text-center"> <?= Html::a("Usuario: ".$model->getIdUsu()->one()->username, ['user/profile/show', 'id' => $model->id_usu], ['class' => '']) ?> </div>
            <div class="row articulo-descripciontitulo naranjamp"><?= "<h1>DESCRIPCION</h1>" ?></div>
            <div class="row"> <?= $model->descripcion ?> </div> 
            <div class="row articulo-preguntas naranjamp"> <h1>PREGUNTAS</h1></div>
            <div class="row formupreguntas">
                   <?php $form = ActiveForm::begin(["method" => "post", "enableClientValidation" => true, "options" => ["enctype" => "multipart/form-data", 'class'=>'col-md-12'],
                    ]);     ?> 
            <?= $form->field($preguntas, 'pregunta')->textInput(['maxlength' => true],['class'=>'col-md-6']);  ?>
            <div class="form-group articulo-formulario-pregunta">
                    <div class="col-lg-offset-1 col-lg-11 botonpreguntar">
                    <?php if (!Yii::$app->user->isGuest): ?>
                        <?php if(Yii::$app->user->identity->getId() != $model->id_usu): ?>
                        <?= Html::submitButton('Preguntar', ['class' => 'btn btn-info']) ?>
                        <?php else: ?>
                        <?= Html::submitButton('Preguntar', ['class' => 'btn btn-info', 'disabled' => true]) ?>
                        <?php endif; ?>
    <?php else: ?>
                    <?= "Para Preguntar debes Iniciar Sesion".Html::submitButton('Preguntar', ['class' => 'btn btn-info', 'disabled' => true]) ?>
<?php endif; ?>
                        
                    </div>
                </div> 
            <?php ActiveForm::end() ?>
            </div>  <!-- formupregunta 
            select(['id', 'categoria'])->-->
            <!-- </div> -->
            <div class="row cuadro-preugntas">
            <?php if (!$mpreguntas): ?>
                <div class="sinpreguntas">aun nadie hace preguntas... ¡Haz Tu la primera!</div>
                <?php else: ?>
                <?php foreach ($mpreguntas as $key => $value): ?>
                    
                    
                <div class="pregunta"> <span class="glyphicon glyphicon-shopping-cart"></span> <?php echo $value['pregunta'] ?></div>
                <div class="respuesta">
                <span class="glyphicon glyphicon glyphicon-user"></span>
                <?php if (is_null($value['respuesta'])): ?>
                     El vendedor aun no ha respondido
                 <?php else: ?>
                 <?= $value['respuesta'] ?>
                <?php endif ?>
                </div>
               
            <?php endforeach;?>

                <?php endif ?>
            </div>
        </div>
        <div class="col-md-3 articulo">   
            <h1 class="articulo-articulo"><?= Html::encode($this->title) ?></h1>
            <?php if (!Yii::$app->user->isGuest): ?>
                <?php if (Yii::$app->user->identity->getId() == $model->id_usu ): ?>
                <?php //echo Html::a("Editar mi Publicacion", Url::to(['update', 'id' => $model['id']])); ?>
                <?php endif ?>
            <?php endif ?>
                <div class="art_precio"><?php echo $model->precio." Bs"; ?></div>
                <div class="art_cantidad"><?php echo "Cant: ".$model->stock;?></div>
            <?php if (!isset($model)): ?> 
                <?php if (Yii::$app->user->identity->getId() != $model->id_usu ): ?><?php endif ?>
                <div class="art_comprar"> <?= Html::submitButton(Yii::t('app', 'buy'), ['class' => $model->isNewRecord ? 'disabled btn btn-lg btn-info' : 'btn btn-info']) ?> </div>
            <?php endif ?>
        </div>
    </div>
</div>
   
<?php
$js = <<<JS
// get the form id and set the event
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
   var \$form = $(this);
   alert('No se pueden hacer preguntas por los momentos');
}).on('submit', function(e){
    e.preventDefault();
});
JS;
 
// $this->registerJs($js); 
















































