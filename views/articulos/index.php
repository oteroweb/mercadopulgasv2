<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ArticulosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Mis Articulos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articulos-index">


    <h1 class="articulos-create impact text-center"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="text-center">
        <?= Html::a(Yii::t('app', 'Vender'), ['create'], ['class' => 'btn btn-info']) ?>
    </p>
    <div class="row">
        <div class="col-sm-3 col-md-3 sidebar"> 
            <?= "<ul class='categoriasinicio'>"; ?>
            <?php  foreach ($categories as $key => $value){
                ?> 
                <li><a href=" <?=  Url::to(['articulos/index', 'cat' => $value['id']]) ?>"> <?= $value['categoria']; ?>
                </a></li> <?php } echo "</ul>";
                 ?>  

        </div>
        <div class="col-sm-9 col-md-9 "> 
            <!-- <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">  -->

            <div class="row">
            <?php if (!count($models)): ?>
                no hay articulos
            <?php endif ?>
            <?php foreach ($models as $model) :?>
                    <div class="col-md-4 articulo"> 
                        <?php if (!$model->foto1): ?>
                        <div class="art_foto1">  
                        <?= Html::img('@web/images/sinfoto.png',['width'=>'100%','height'=>'100%','title'=>$model->articulo]);?>
                        </div>
                        <?php else: ?>
                        <div class="art_foto1"> 
                           <?= Html::img('@web/images/articulos/'.$model->foto1,['width'=>'100%','height'=>'100%','title'=>$model->articulo]);?>
                        </div>
                         <?php endif ?>
                        <div class="art_articulo"><?php echo $model->articulo;?></div>
                        <div class="art_precio"><?php echo $model->precio." Bs"; ?></div>
                        <?= Html::a("<i class = 'editart glyphicon glyphicon-pencil'></i>", ['update', 'id' => $model->id]) ?>
                        <?= Html::a("<i class = 'verart glyphicon glyphicon-eye-open'></i>", ['view', 'id' => $model->id]) ?>       
                        <?= Html::a("", ['articulos/delete', 'id'=>$model->id], [ 'class' => 'verelim glyphicon glyphicon-trash', 'data' => [ 'confirm' => "Estas Seguro que deseas Eliminar Este articulo, Esta Accion no se puede deshacer?", 'method' => 'post', ], ]); ?>
                    </div>
                       
            <?php endforeach; ?>
            </div>
            <div class="row">
                <div class="col-md-12 paginacion" style="text-align: center;">
                    <?php echo LinkPager::widget(['pagination' => $pages,]);?>
                </div>

            </div>


    </div>
</div>
</div>
