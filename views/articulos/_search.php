<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ArticulosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articulos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'articulo') ?>

    <?= $form->field($model, 'subtitulo') ?>

    <?= $form->field($model, 'foto1') ?>

    <?= $form->field($model, 'foto2') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'ventas') ?>

    <?php // echo $form->field($model, 'stock') ?>

    <?php // echo $form->field($model, 'precio') ?>

    <?php // echo $form->field($model, 'tipopago') ?>

    <?php // echo $form->field($model, 'id_usu') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'garantia') ?>

    <?php // echo $form->field($model, 'tipopublicacion') ?>

    <?php // echo $form->field($model, 'fechaexp') ?>

    <?php // echo $form->field($model, 'fechainc') ?>

    <?php // echo $form->field($model, 'id_cat') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
