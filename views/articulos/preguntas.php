<?php

use yii\helpers\Html;
use app\models\Preguntas;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

// use app\models\Articulos;
// use app\models\Preguntas;
// use app\models\ArticulosSearch;
// use yii\web\Controller\PreguntasController;
// use yii\web\NotFoundHttpException;
// use yii\filters\VerbFilter;
// use app\models\Categorias;
// use yii\helpers\ArrayHelper;
// use yii\filters\AccessControl;
// use yii\web\UploadedFile;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ArticulosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Mis Preguntas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articulos-index">
<?php $categories = null;  ?>

    <h1 class="articulos-create impact text-center"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<hr>
<?php if ($my): ?>
    <?php foreach ($my as $key => $value) : ?>
        <div class="row">
        <div> <h1> <?php echo $value['pregunta']; ?> </h1></div>
        <div> 
            <?php if (is_null($value['respuesta'])): ?>
                <?php $form = ActiveForm::begin(["method" => "post", "enableClientValidation" => true, "options" => ["enctype" => "multipart/form-data", 'class'=>'col-md-12'], ]);     ?> 
                <?= $form->field($preguntas, 'id')->textInput( ['readonly' => true, 'value' => $value['id'], 'maxlength' => true],['class'=>'col-md-6'])->label(false);  ?>
                <?= $form->field($preguntas, 'respuesta')->textInput(['maxlength' => true],['class'=>'col-md-6'])->label(false);  ?>

                <?= Html::submitButton('Responder', ['class' => 'btn btn-info']) ?>

                <?php ActiveForm::end() ?>
                <?php else: ?>
                    <?= $value['respuesta'] ?>
                <?php endif; ?></div>
            <hr>
            </div>
        <?php endforeach; ?>
<?php else: ?>

    no tienes preguntas 

<?php endif ?>


</div>



