<?php

use kartik\widgets\FileInput;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\widgets\ActiveForm;

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model frontend\models\Articulos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articulos-form">
<div class="col-md-3"> </div>
    <?php // $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?php $form = ActiveForm::begin(["method" => "post",
         "enableClientValidation" => true,
         "options" => ["enctype" => "multipart/form-data", 'class'=>'col-md-6'],
         ]);
    ?>
    <?php   

     // echo Html::activeLabel($model,'fieldname'); //label
        // echo Html::activeTextInput($model, 'fieldname'); //Field
        // echo Html::error($model,'fieldname', ['class' => 'help-block']);

         ?>
    <?= $form->field($model, 'id_cat')->dropDownList($categoria,['prompt'=>'Seleccione una categoria']); ?>
<?= $form->field($model, 'articulo')->textInput(['maxlength' => true]) ?>
    <div class="form-group field-articulos-stock required has-error">   
    <?php 
  // echo Html::activeLabel($model,'articulo'); //label
  //       echo Html::activeTextInput($model, 'articulo'); //Field
  //       echo Html::error($model,'articulo', []);



    $form->field($model, 'articulo')->textInput(['maxlength' => true],['class'=>'col-md-6']) ?>
    </div>
    
<?php 
echo FileInput::widget([ 
'model' => $model, 
'attribute' => 'foto1', 
'options' => ['multiple' => false],
 'pluginOptions' => [   'showUpload' => false,  'browseLabel' => '',
        'removeLabel' => '','initialPreview'=>[ Html::img("images/articulos/".$model->foto1,
 ['class'=>'file-preview-image', 'alt'=>'Adjuntar Foto', 'title'=>'Adjuntar Foto']), ], 'initialCaption'=>$model->articulo,] ]); 

 ?>
    
<?= $form->field($model, 'descripcion')->widget(CKEditor::className(), [        'options' => ['rows' => 6],         
  'preset' => 'standard',  
'clientOptions' => []     ]) ?>


    <!-- </div> -->
    <?= $form->field($model, 'estado')->radioList(array(123=>'Nuevo',1231=>'Usado')); ?>
    <?php echo $form->field($model, 'precio')->input('number',['step'=>"0.01"]); ?>
    <?= $form->field($model, 'stock')->textInput() ?>
    <?php // $form->field($model, 'tipopago')->textInput(['maxlength' => true]) ?>
    <?php // $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

<?php ?>




    <?= $form->field($model, 'garantia')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-lg btn-info' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div> 
<div class="row"></div>