<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username') ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'password_repeat')->passwordInput() ?>
                <?= $form->field($model, 'doc') ?>

                <?= $form->field($model, 'telf') ?>
                <?= $form->field($model, 'telf2') ?>
                <?= $form->field($model, 'direccion') ?>
                <?= $form->field($model, 'estado')->dropDownList(array('Amazonas' =>'Amazonas', 'Anzoátegui' =>'Anzoátegui', 'Apure' =>'Apure', 'Aragua' =>'Aragua', 'Barinas' =>'Barinas', 'Bolívar' =>'Bolívar', 'Carabobo' =>'Carabobo', 'Cojedes' =>'Cojedes', 'Delta Amacuro' =>'Delta Amacuro', 'Falcón' =>'Falcón', 'Distrito Capital' =>'Distrito Capital', 'Guárico' =>'Guárico', 'Lara' =>'Lara', 'Mérida' =>'Mérida', 'Miranda' =>'Miranda', 'Monagas' =>'Monagas', 'Nueva Esparta' =>'Nueva Esparta', 'Portuguesa' => 'Portuguesa', 'Sucre' =>'Sucre', 'Táchira' =>'Táchira', 'Trujillo' =>'Trujillo', 'Vargas' =>'Vargas', 'Yaracuy' =>'Yaracuy', 'Zulia' =>'Zulia')) ?>
                <?= $form->field($model, 'ciudad') ?>
        

                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

    <!-- - 'Amazonas' =>'Amazonas', 'Anzoátegui' =>'Anzoátegui', 'Apure' =>'Apure', 'Aragua' =>'Aragua', 'Barinas' =>'Barinas', 'Bolívar' =>'Bolívar', 'Carabobo' =>'Carabobo', 'Cojedes' =>'Cojedes', 'Delta Amacuro' =>'Delta Amacuro', 'Falcón' =>'Falcón', 'Distrito' Capital =>'Distrito Capital', 'Guárico' =>'Guárico', 'Lara' =>'Lara', 'Mérida' =>'Mérida', 'Miranda' =>'Miranda', 'Monagas' =>'Monagas', 'Nueva' Esparta =>'Nueva Esparta', 'Portuguesa' => 'Portuguesa', 'Sucre' =>'Sucre', 'Táchira' =>'Táchira', 'Trujillo' =>'Trujillo', 'Vargas' =>'Vargas', 'Yaracuy' =>'Yaracuy', 'Zulia' =>'Zulia',  -->