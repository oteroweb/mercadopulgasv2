<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\bootstrap\Carousel;
// use yii\widgets\ActiveForm;
// use yii\helpers\Url;

$this->title = 'Bienvenidos a Mercadopulgas';
?>

<div class="site-index">
<div class="carousel-index">
  
</div>
    <div class="mensaje-index">
        <!-- <h1>Mercadopulgas!</h1> -->

        <p class="lead">¡Comienza a Vender Ahora! <a class="btn btn-md btn-info" href="<?= Url::to(array('articulos/create'))?>">Vender</a></p>
    </div>

    
    <div class="row">
        <div class="col-sm-3 col-md-3 sidebar"> 
            <?= "<ul class='categoriasinicio'>"; ?>
            <?php  foreach ($categories as $key => $value){
                ?> <li><a href=" <?=  Url::to(['index', 'cat' => $value['id']]) ?>"> <?= $value['categoria']; ?>
                </a></li> <?php } echo "</ul>";
                 ?>  

        </div>
        <div class="col-sm-9 col-md-9 "> 
            <!-- <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">  -->

            <div class="row">
            <?php if (!count($models)): ?>
                no hay articulos
            <?php endif ?>
            <?php foreach ($models as $model) :?>
                <a href="<?php  echo "index.php?r=articulos/view&id=".$model->id; ?>">
                    <div class="col-md-4 articulo"> 
                        <?php if (!$model->foto1): ?>
                        <div class="art_foto1">  
                        <?= Html::img('@web/images/sinfoto.png',['width'=>'100%','height'=>'100%','title'=>$model->articulo]);?>
                        </div>
                        <?php else: ?>
                        <div class="art_foto1"> 
                           <?= Html::img('@web/images/articulos/'.$model->foto1,['width'=>'100%','height'=>'100%','title'=>$model->articulo]);?>
                        </div>
                         <?php endif ?>
                        <div class="art_articulo impact"><?php echo $model->articulo;?></div>
                        <div class="art_precio"><?php echo $model->precio." Bs"; ?></div>
                    </div>
                        </a>
                       
            <?php endforeach; ?>
            </div>
            <div class="row">
                <div class="col-md-12 paginacion" style="text-align: center;">
                    <?php echo LinkPager::widget(['pagination' => $pages,]);?>
                </div>

            </div>
        

    </div>
</div>