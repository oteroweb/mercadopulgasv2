<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

<h1><?= 'Pagina no Encontrada' ?></h1>
    <div class="alert alert-danger">
      <h2>  <?= nl2br(Html::encode($message)) ?></h2>
    </div>

    <p>
        Error en su requerimiento
    </p>
    <p>
        Si el problema persiste por favor contactar al equipo tecnico de Mercadopulgas <a href="javascript:void(window.open('http://www.mercadopulgas.com.ve/soporte/chat.php?a=2b5f5','','width=610,height=760,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))">Aqui</a>
    </p>

</div>
