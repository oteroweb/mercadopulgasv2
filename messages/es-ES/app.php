<?php 
return [
'captcha'=> 'Codigo de Validacion',
'buy'=>'Comprar',
    'Create Articulos'=> 'Vender mi Producto',
    'Articulo' => 'Titulo del Articulo',
    'Heading' => 'título',
    'My Yii Application' => 'Mi aplicación Yii',
    'Yii Documentation' => 'Yii Documentación',
    'Yii Extensions' => 'Extensiones Yii',
    'Yii Forum' => 'Yii Foro',
    'Are you sure you want to delete this item?' => '¿Seguro que quieres borrar este artículo?',
    'Congratulations!' => '¡Enhorabuena!',
    'Create' => 'crear',
    'Create {modelClass}' => 'crear {modelClass}',
    'Created At' => 'Creado el',
    'Delete' => 'borrar',
    'ID' => 'identificación',
    'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit , sed no tempor eiusmod ut labore et dolore incididunt magna aliqua . Ut enim ad minim veniam , nostrud quis ullamco exercitation nisi ut laboris aliquip commodo ex ea consequat . Duis Aute Irure dolor en reprehenderit en voluptate velita esse cillum dolore eu nulla fugiat pariatur .',
    'Message' => 'mensaje',
    'Permissions' => 'Permisos',
    'Reset' => 'reajustar',
    'Search' => 'búsqueda',
    'Statuses' => 'Los estados',
    'Update' => 'actualización',
    'Update {modelClass}: ' => 'actualización {modelClass} :',
    'Updated At' => 'Actualizado A',
    'You have successfully created your Yii-powered application.' => 'Ha creado su aplicación Yii con alimentación.',
    'Subtitulo' => '',
    'Foto1' => 'Foto 1',
    'Foto2' => 'Foto 2',
    'Estado' => 'Estado',
    'Ventas' => 'Vendidos',
    'Stock' => 'Cantidad',
'Precio' => 'Precio',
'Tipopago' => 'Tipo de Pago',

'Id Usu' => 'Usuario',

'Descripcion' => 'Descripcion de mi Producto',

'Garantia' => 'Garantia',

'Tipopublicacion' => 'Tipo de Publicacion',

'Fechaexp' => 'Fecha de cierre',

'Fechainc' => 'Fecha de inicio',

'Id Cat' => 'Categoria',


'nombre'=> 'Nombre',
'password_repeat'=> 'Repetir Contraseña',
'apellido'=> 'Apellido',
'cedula'=> 'Cedula o Rif',
'direccion'=> 'Direccion',
'telefono'=> 'Telefono',
'estado'=> 'Estado',
// 'captcha'=> 'Verificacion',

];

 ?>