<?php

$params = require(__DIR__ . '/params.php');

$config = [
 'language' => 'es-ES',
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
    'user' => [
        'class' => 'dektrium\user\Module',
        'enableUnconfirmedLogin'=>true,
		'admins' => ['jilio1990'],
        // 'enableConfirmation'=>false,
        // 'urlPrefix'=>'usuario',
        // 'controllerMap' => [ 
        // 'registration' => [
        // 'class' => \dektrium\user\controllers\RegistrationController::className(),
        // ],
        // 'admin' => 'app\controllers\user\AdminController'
                        // ],
        // 'modelMap' => [
        //     'User' => 'app\models\User',
        //     'RegistrationForm' => 'app\models\RegistrationForm',
        // ],

        //   'controllerMap' => [
        //     'profile' => 'app\controllers\ProfileController',
        //     'settings' => 'app\controllers\SettingsController'
        // ],
    ],
],
    'components' => [
  'authClientCollection' => [
        'class'   => \yii\authclient\Collection::className(),
        'clients' => [
            // here is the list of clients you want to use
            // you can read more in the "Available clients" section


        'facebook' => [
    'class'        => 'dektrium\user\clients\Facebook',
    'clientId'     => '200722763599471',
    'clientSecret' => 'e670148693b6791998bce346c43a6fea',
],
        'twitter' => [ 'class' => 'dektrium\user\clients\Twitter', 'consumerKey'    => 'fTYRJkTy01yYFAUuT0Z010Hsz', 'consumerSecret' => '0Kc7nguq4UeVzg3KrhUDXOiEyB07fauPeBQpLQtocUCtlH1phR', ],
    'google' => [ 
'class'=> 'dektrium\user\clients\Google', 
'clientId' => '853061795754-she6j1l1lost34ctrmfv7oaa40fofavn.apps.googleusercontent.com', 
'clientSecret' => 'oo9ZUFXWoL6eOvAaaEzVpBnQ', ],
        ],
    ],
		'view' => [
		        'theme' => [
		            'pathMap' => [
		                '@dektrium/user/views' => '@app/views/user'
		            ],
		        ],
		    ],


    	'i18n' => [
        	'translations' => [
            	'app*' => [      // my other translation file
                	'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                    'app' => 'app.php',
                    	    ],
                        ],
                    'user*' => [ // 
                    	'class' => 'yii\i18n\PhpMessageSource',
                        'basePath' => '@app/messages', // my custom message path.
                        'sourceLanguage' => 'en-US',
                        'fileMap' => [
                        	'user' => 'user.php', // I put this file on folder common/messages/ms/user.php so yours zh-CN
                            ],
                        ]
                        ],
                    ],


        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'XfmqLcLk-LrkX9i-Lc-mup-vyWcHHPwM',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        // 'user' => [
            // 'identityClass' => 'app\models\User',
            // 'enableAutoLogin' => true,
        // ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
