<?php

use yii\db\Migration;
use yii\db\Schema;

class m151207_040343_preguntas extends Migration
{
    public function up()
    {   $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
 $this->createTable('preguntas', [
            'id' => Schema::TYPE_PK,
            'pregunta' => Schema::TYPE_STRING . '(255) NOT NULL',
            'respuesta' => Schema::TYPE_STRING . '(255)',
            'id_pre' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'id_rep' => Schema::TYPE_INTEGER . '(11)',
            'id_art' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'FOREIGN KEY ([[id_pre]]) REFERENCES user ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_rep]]) REFERENCES user ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_art]]) REFERENCES articulos ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);
    }    


    public function down()
    {
        $this->dropTable('preguntas');
    }
}
