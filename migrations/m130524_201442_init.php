<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

            $this->addColumn('{{%user}}', 'doc',  $this->string(32)->notNull());
            $this->addColumn('{{%user}}', 'telf',  $this->string(32)->notNull());
            $this->addColumn('{{%user}}', 'telf2',  $this->string(32)->notNull());
            $this->addColumn('{{%user}}', 'direccion',  $this->string(32)->notNull());
            $this->addColumn('{{%user}}', 'estado',  $this->string(32)->notNull());
            $this->addColumn('{{%user}}', 'ciudad',  $this->string(32)->notNull());
            $this->addColumn('{{%user}}', 'password_reset_token',  $this->string()->unique());
            $this->addColumn('{{%user}}', 'status',  $this->smallInteger()->notNull()->defaultValue(10));
        $this->insert('{{%user}}', 
            [ 
            // 'id' => 1,
            'username' =>'jilio1990',
            'auth_key' =>'5E08CYOZSsESSOjP0mpAcM7KiPgIsRhx',
            // 'doc' => 'V19708951',
            // 'telf' => '04167528772',
            // 'telf2' =>'02512414808',
            // 'direccion' =>'barquisimeto',
            // 'estado' =>'Lara',e
            // 'ciudad' =>'Barquisimeto',
            'password_hash' =>'$2y$13$Uk1Pek8P9Hasqd.R.ZJsHOYlnDiHY0TKbJZe8IPt3oU6g73R5Dau2',
            // 'password_reset_token' =>NULL,
            'email' =>'admin@mercadopulgas.com.ve',

            // 'status' =>10,
            'created_at' =>'1447228597',
            'updated_at' =>'1447228597',
            ]);
    }


       


    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
