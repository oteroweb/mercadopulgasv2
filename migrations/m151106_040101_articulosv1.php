<?php

use yii\db\Schema;

class m151106_040101_articulosv1 extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        

  $this->createTable('categorias', [
            'id' => Schema::TYPE_PK,
            'categoria' => Schema::TYPE_STRING . '(255)',
            'padre' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'status' => Schema::TYPE_STRING . ' NOT NULL DEFAULT 1',
        ], $tableOptions);
   $this->insert('categorias', [ 'categoria' => 'Accesorios para Vehículos', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Bebés', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Animales y Mascotas', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Hogar y Muebles', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Industrias', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Instrumentos Musicales', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Cámaras y Accesorios', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Celulares y Teléfonos', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Coleccionables y Hobbies', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Computación', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Consolas y Videojuegos', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Deportes y Fitness', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Electrodomésticos', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Electrónica, Audio y Video', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Juegos y Juguetes', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Libros, Música y Películas', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Relojes, Joyas y Bisutería', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Ropa, Zapatos y Accesorios', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Salud y Belleza', 'padre' => '0', 'status' => '1',]);
   $this->insert('categorias', [ 'categoria' => 'Otras Categorías', 'padre' => '0', 'status' => '1',]);

    $this->createTable('articulos', [
            'id' => Schema::TYPE_PK,
            'articulo' => Schema::TYPE_STRING . '(255) NOT NULL',
            'subtitulo' => Schema::TYPE_TEXT,
            'foto1' => Schema::TYPE_STRING . '(255)',
            'foto2' => Schema::TYPE_STRING . '(255)',
            'estado' => Schema::TYPE_STRING . ' NOT NULL',
            'ventas' => Schema::TYPE_INTEGER . '(5) NOT NULL',
            'stock' => Schema::TYPE_INTEGER . '(3) NOT NULL',
            'precio' => Schema::TYPE_FLOAT . '(20) NOT NULL',
            'tipopago' => Schema::TYPE_STRING . '(255) NOT NULL',
            'id_usu' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'descripcion' => Schema::TYPE_TEXT,
            'garantia' => Schema::TYPE_STRING . '(30) NOT NULL',
            'tipopublicacion' => Schema::TYPE_STRING . ' NOT NULL',
            'fechaexp' => Schema::TYPE_DATE . ' NOT NULL',
            'fechainc' => Schema::TYPE_DATE . ' NOT NULL',
            'id_cat' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'FOREIGN KEY ([[id_cat]]) REFERENCES categorias ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[id_usu]]) REFERENCES user ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('articulos');
        $this->dropTable('categorias');
    }
}
