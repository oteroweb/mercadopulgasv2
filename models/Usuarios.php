<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property integer $id
 * @property string $email
 * @property string $fname
 * @property string $lname
 * @property string $direccion
 * @property string $pass
 * @property string $date
 * @property string $expdate
 * @property string $status
 * @property string $user_type
 * @property string $usuario
 * @property string $telefono
 * @property integer $documento
 * @property integer $id_ciu
 * @property double $balance
 *
 * @property Articulos[] $articulos
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'fname', 'lname', 'direccion', 'pass', 'date', 'expdate', 'status', 'user_type', 'usuario', 'telefono', 'documento', 'id_ciu', 'balance'], 'required'],
            [['date', 'expdate'], 'safe'],
            [['status', 'user_type'], 'string'],
            [['documento', 'id_ciu'], 'integer'],
            [['balance'], 'number'],
            [['email', 'fname', 'lname', 'direccion', 'pass', 'usuario'], 'string', 'max' => 255],
            [['telefono'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'fname' => Yii::t('app', 'Fname'),
            'lname' => Yii::t('app', 'Lname'),
            'direccion' => Yii::t('app', 'Direccion'),
            'pass' => Yii::t('app', 'Pass'),
            'date' => Yii::t('app', 'Date'),
            'expdate' => Yii::t('app', 'Expdate'),
            'status' => Yii::t('app', 'Status'),
            'user_type' => Yii::t('app', 'User Type'),
            'usuario' => Yii::t('app', 'Usuario'),
            'telefono' => Yii::t('app', 'Telefono'),
            'documento' => Yii::t('app', 'Documento'),
            'id_ciu' => Yii::t('app', 'Id Ciu'),
            'balance' => Yii::t('app', 'Balance'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticulos()
    {
        return $this->hasMany(Articulos::className(), ['id_usu' => 'id']);
    }
}
