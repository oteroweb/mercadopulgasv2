<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property integer $confirmed_at
 * @property string $unconfirmed_email
 * @property integer $blocked_at
 * @property string $registration_ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 *
 * @property Articulos[] $articulos
 * @property Profile $profile
 * @property SocialAccount[] $socialAccounts
 * @property Token[] $tokens
 */
// namespace app\models;

use dektrium\user\models\User as BaseUser;

class User extends BaseUser
{
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        $scenarios['create'][]   = 'nombre';
        $scenarios['update'][]   = 'nombre';
        $scenarios['register'][] = 'nombre';

        $scenarios['create'][]   = 'apellido';
        $scenarios['update'][]   = 'apellido';
        $scenarios['register'][] = 'apellido';

         $scenarios['create'][]   = 'cedula';
        $scenarios['update'][]   = 'cedula';
        $scenarios['register'][] = 'cedula';

         $scenarios['create'][]   = 'direccion';
        $scenarios['update'][]   = 'direccion';
        $scenarios['register'][] = 'direccion';

         $scenarios['create'][]   = 'telefono';
        $scenarios['update'][]   = 'telefono';
        $scenarios['register'][] = 'telefono';
           $scenarios['create'][]   = 'estado';
        $scenarios['update'][]   = 'estado';
        $scenarios['register'][] = 'estado';

        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();
        // add some rules
        $rules['nombreRequired'] = ['nombre', 'required'];
        $rules['nombreLength']   = ['nombre', 'string', 'max' => 10];

        // $rules['captchaRequired'] = ['captcha', 'required'];
        // $rules['captchaLength']   = ['captcha', 'captcha'];

        return $rules;
    }
    // public function register()
    // {
    //     // do your magic
    // }
}
