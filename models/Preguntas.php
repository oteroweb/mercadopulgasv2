<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "preguntas".
 *
 * @property integer $id
 * @property string $pregunta
 * @property string $respuesta
 * @property integer $id_pre
 * @property integer $id_rep
 * @property integer $id_art
 *
 * @property User $idPre
 * @property User $idRep
 * @property Articulos $idArt
 */
class Preguntas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'preguntas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pregunta'], 'required'],
            [['id_pre', 'id_rep', 'id_art'], 'integer'],
            // [['pregunta', 'respuesta'], 'string', 'max' => 255],
            [['id_pre'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_pre' => 'id']],
            [['id_rep'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_rep' => 'id']],
            [['id_art'], 'exist', 'skipOnError' => true, 'targetClass' => Articulos::className(), 'targetAttribute' => ['id_art' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pregunta' => Yii::t('app', 'Pregunta'),
            'respuesta' => Yii::t('app', 'Respuesta'),
            'id_pre' => Yii::t('app', 'Id Pre'),
            'id_rep' => Yii::t('app', 'Id Rep'),
            'id_art' => Yii::t('app', 'Id Art'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPre()
    {
        return $this->hasOne(User::className(), ['id' => 'id_pre']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRep()
    {
        return $this->hasOne(User::className(), ['id' => 'id_rep']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdArt()
    {
        return $this->hasOne(Articulos::className(), ['id' => 'id_art']);
    }

    /**
     * @inheritdoc
     * @return PreguntasQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PreguntasQuery(get_called_class());
    }

       public function beforeSave($insert)
            {
                if (parent::beforeSave($insert)) {
                   if ($this->isNewRecord) {
 // * @property string $pregunta
 // * @property string $respuesta
 // * @property integer $id_rep
                       $this->id_pre=Yii::$app->user->identity->getId();
                       $this->id_art=$_GET['id'];
                      
                   }

                    return true;
                } else {
                    return false;
                }
             }
}
