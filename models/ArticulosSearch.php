<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Articulos;

/**
 * ArticulosSearch represents the model behind the search form about `frontend\models\Articulos`.
 */
class ArticulosSearch extends Articulos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ventas', 'stock', 'precio', 'id_usu', 'id_cat'], 'integer'],
            [['articulo', 'subtitulo', 'foto1', 'foto2', 'estado', 'tipopago', 'descripcion', 'garantia', 'tipopublicacion', 'fechaexp', 'fechainc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Articulos::find()->where(['id_usu' => Yii::$app->user->identity->getId()]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ventas' => $this->ventas,
            'stock' => $this->stock,
            'precio' => $this->precio,
            'id_usu' => $this->id_usu,
            'fechaexp' => $this->fechaexp,
            'fechainc' => $this->fechainc,
            'id_cat' => $this->id_cat,
        ]);

        $query->andFilterWhere(['like', 'articulo', $this->articulo])
            ->andFilterWhere(['like', 'subtitulo', $this->subtitulo])
            ->andFilterWhere(['like', 'foto1', $this->foto1])
            ->andFilterWhere(['like', 'foto2', $this->foto2])
            ->andFilterWhere(['like', 'estado', $this->estado])
            ->andFilterWhere(['like', 'tipopago', $this->tipopago])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'garantia', $this->garantia])
            ->andFilterWhere(['like', 'tipopublicacion', $this->tipopublicacion]);

        return $dataProvider;
    }
}
