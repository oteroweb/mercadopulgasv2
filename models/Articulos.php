<?php

namespace app\models;
use yii\web\UploadedFile;
use Yii;

/**
 * This is the model class for table "articulos".
 *
 * @property integer $id
 * @property string $articulo
 * @property string $subtitulo
 * @property string $foto1
 * @property string $foto2
 * @property string $estado
 * @property integer $ventas
 * @property integer $stock
 * @property double $precio
 * @property string $tipopago
 * @property integer $id_usu
 * @property string $descripcion
 * @property string $garantia
 * @property string $tipopublicacion
 * @property string $fechaexp
 * @property string $fechainc
 * @property integer $id_cat
 *
 * @property Categorias $idCat
 * @property User $idUsu
 */
class Articulos extends \yii\db\ActiveRecord
{
    // public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articulos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

/*
 ['file', 'file','skipOnEmpty' => false,
       'uploadRequired' => 'No has seleccionado ningún archivo', //Error
       'maxSize' => 1024*1024*1, //1 MB
       'tooBig' => 'El tamaño máximo permitido es 1MB', //Error
       'minSize' => 10, //10 Bytes
       'tooSmall' => 'El tamaño mínimo permitido son 10 BYTES', //Error
       // 'extensions' => 'pdf, txt, doc',
       'wrongExtension' => 'El archivo {file} no contiene una extensión permitida {extensions}', //Error
       'maxFiles' => 4,
       'tooMany' => 'El máximo de archivos permitidos son {limit}', //Error
       ],*/
            [['articulo', 'estado', 'stock', 'precio', 'garantia',  'id_cat'], 'required','message'=>'{attribute} no puede estar en blanco .'],
            [['subtitulo', 'descripcion'], 'string'],
            [['ventas', 'stock', 'id_usu', 'id_cat'], 'integer'],
            [['fechaexp', 'fechainc','foto1'], 'safe'],
            [['articulo', 'estado', 'tipopago', 'tipopublicacion'], 'string', 'max' => 255],
            [['garantia'], 'string', 'max' => 30],
            [['id_cat'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['id_cat' => 'id']],
            [['id_usu'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_usu' => 'id']],
            [['foto1'], 'file', 'extensions' => 'png, jpg'],

             // [['foto1'], 'safe'],
            // [['foto1'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'articulo' => Yii::t('app', 'Articulo'),
            'subtitulo' => Yii::t('app', 'Subtitulo'),
            'foto1' => Yii::t('app', 'Foto1'),
            'foto2' => Yii::t('app', 'Foto2'),
            'estado' => Yii::t('app', 'Estado'),
            'ventas' => Yii::t('app', 'Ventas'),
            'stock' => Yii::t('app', 'Stock'),
            'precio' => Yii::t('app', 'Precio'),
            'tipopago' => Yii::t('app', 'Tipopago'),
            'id_usu' => Yii::t('app', 'Id Usu'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'garantia' => Yii::t('app', 'Garantia'),
            'tipopublicacion' => Yii::t('app', 'Tipopublicacion'),
            'fechaexp' => Yii::t('app', 'Fechaexp'),
            'fechainc' => Yii::t('app', 'Fechainc'),
            'id_cat' => Yii::t('app', 'Id Cat'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCat()
    {
        return $this->hasOne(Categorias::className(), ['id' => 'id_cat']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsu()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usu']);
    }

        public function beforeSave($insert)
            {
                if (parent::beforeSave($insert)) {
                   if ($this->isNewRecord) {
                       // $this->articulo= "nuevo";
                       $this->id_usu=Yii::$app->user->identity->getId();
                       $this->ventas=0;
                      
                   }

                    return true;
                } else {
                    return false;
                }
             }   

     public function upload()
    {

        // if ($this->isNewRecord) {

            if ($this->validate()) {
                $rnd= rand(0,9999);
                // $rnd = 2;
                $this->foto1->saveAs('images/articulos/'.$rnd.$this->foto1->baseName . '.' . $this->foto1->extension);
                 $this->foto1= $rnd.$this->foto1->name;
                return true;
            } else {
                return false;
            }
        // }

        // else {

        // }
    }
     public function upload2()
    {
        if ($this->validate()) {
            $rnd= rand(0,9999);
            $this->foto2->saveAs('images/articulos/'.$rnd.$this->foto2->baseName . '.' . $this->foto2->extension);
             $this->foto2= $rnd.$this->foto2->name;
             return true;
        } else {
            return false;
        }
    }
}
