<?php
namespace app\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $doc;
    public $telf;
    public $telf2;
    public $direccion;
    public $estado;
    public $ciudad;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
  
            [['username', 'doc', 'direccion', 'estado', 'ciudad', 'email'], 'required'],
            // ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Contraseña no coinciden" ], 
            [['direccion', 'estado', 'ciudad'], 'string', 'max' => 120],

        ];
/*
            */

    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->doc= $this->doc;
            $user->telf= $this->telf;
            $user->telf2= $this->telf2;
            $user->direccion= $this->direccion;
            $user->estado= $this->estado;
            $user->ciudad= $this->ciudad;
            $user->setPassword($this->password);
            $user->generateAuthKey();

            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
     public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Usuario',
            'doc' => 'Cedula/Rif',
            'telf' => 'Telefono Personal',
            'telf2' => 'Telefono Casa/oficina',
            'direccion' => 'Direccion',
            'estado' => 'Estado',
            'ciudad' => 'Ciudad',
            'email' => 'Correo Electronico',
            'password_repeat' => 'Repetir Contraseña',
            'password' => 'Contraseña',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
