<?php
namespace app\models;

use dektrium\user\models\Profile;
use dektrium\user\models\RegistrationForm as BaseRegistrationForm;
// use dektrium\user\models\User;
    class RegistrationForm extends \dektrium\user\models\RegistrationForm
    {
        /**
        * @var string
        */
        public $nombre;
        public $captcha;
        public $apellido;
        public $cedula;
        public $direccion;
        public $telefono;
        public $estado;
        public $password_repeat;
            /**
             * @inheritdoc
             */
            public function rules()
            {
                $rules = parent::rules();
            $rules[] = ['nombre', 'required'];
            $rules[] = ['password_repeat', 'required'];
            $rules[] = ['apellido', 'required'];
            $rules[] = ['cedula', 'required'];
            $rules[] = ['direccion', 'required'];
            $rules[] = ['telefono', 'required'];
            $rules[] = ['estado', 'required'];
            $rules[] = ['nombre', 'string', 'max' => 255];
            // $rules[] = ['captcha', 'required'];
            // $rules[] = ['captcha', 'captcha'];
            $rules[] = ['password_repeat', 'compare','compareAttribute'=>'password', 'message'=>"La Contraseñas no son iguales"];
            return $rules;
            }
             public function attributeLabels()
        {
            $labels = parent::attributeLabels();
            $labels['nombre'] = \Yii::t('app', 'nombre');
            $labels['password_repeat'] = \Yii::t('app', 'password_repeat');
            $labels['apellido'] = \Yii::t('app', 'apellido');
            $labels['cedula'] = \Yii::t('app', 'cedula');
            $labels['direccion'] = \Yii::t('app', 'direccion');
            $labels['telefono'] = \Yii::t('app', 'telefono');
            $labels['estado'] = \Yii::t('app', 'estado');
            $labels['captcha'] = \Yii::t('app', 'captcha');
     // return [
                // 'nombre' =>  \Yii::t('app', 'nombre'),
                // 'categoria' =>  \Yii::t('app', 'Categoria'),
                // 'padre' =>  \Yii::t('app', 'Padre'),
                // 'status' =>  \Yii::t('app', 'Status'),
            // ];
            return $labels;
        }

         /**
         * @inheritdoc
         */
        // public function loadAttributes(User $user)
        // {
        //     // here is the magic happens
        //     $user->setAttributes([
        //         'email'    => $this->email,
        //         'username' => $this->username,
        //         'password' => $this->password,
        //     ]);
        //     /** @var Profile $profile */
        //     $profile = \Yii::createObject(Profile::className());
        //     $profile->setAttributes([
        //         'nombre' => $this->nombre,
        //     ]);
        //     $user->setProfile($profile);
        // }
    }
    